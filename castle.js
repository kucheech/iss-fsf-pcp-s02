"use strict";
exports.__esModule = true;
var Castle = /** @class */ (function () {
    function Castle(country, year) {
        this.country = country;
        this.year = year;
        this.version = 1;
    }
    Castle.prototype.toString = function () {
        return "Built in " + this.country + " in " + this.year;
    };
    Castle.prototype.horn = function (times) {
        if (times === void 0) { times = 3; }
        for (var i = 0; i < times; i++) {
            console.log("[Alert]");
        }
    };
    return Castle;
}());
exports.Castle = Castle;
