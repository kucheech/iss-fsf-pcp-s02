import { Horn } from "./horn"

export class Castle implements Horn {

  public size: number;
  public hasMoat: boolean;
  version = 1;

  public constructor(public country: string, public year: number) {

  }

  toString(): string {
    return "Built in " + this.country + " in " + this.year;
  }

  horn(times: number = 3): void {
    for (let i = 0; i < times; i++) {
      console.log("[Alert]");
    }
  }

}
