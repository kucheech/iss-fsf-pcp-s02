//string, number, boolean
let myname:string = 'fred';
// type inference
let age:number = 50;

console.log("My name is %s and am %d year old", myname, age);
