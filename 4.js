var Car = /** @class */ (function () {
    //Constructer
    function Car(make, capacity) {
        this.make = make;
        this.capacity = capacity;
        this.serialNumber = this.make + ":" + this.capacity;
    }
    Car.prototype.horn = function (times) {
        if (times === void 0) { times = 2; }
        for (var i = 0; i < times; i++) {
            console.log("[" + this.make + "]: horn");
        }
    };
    return Car;
}());
//instantiation
var car1 = new Car("Suzuki", 1500); // create instance
car1.horn();
var car2 = new Car("Honda", 1500); // create instance
car2.horn(10);
var garage = [car1, car2];
garage.forEach(car, Car);
{
    car.horn();
}
;
