class Castle {

  public size: number;
  public hasMoat: boolean;

  public constructor(public country:string, public year:number) {

  }

  toString():string {
    return "Built in " + this.country + " in " + this.year;
  }

}

let c:Castle = new Castle("France", 1567);
console.log(c.toString());