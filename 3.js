var c1 = {
    name: "Customer 1",
    email: "c1@gmail.com",
    phone: 12345678,
    edm: false
};
var customers = [];
customers.push(c1);
customers.push({
    name: "Customer 2",
    email: "c2@gmail.com",
    phone: 2222222,
    edm: true,
    remarks: "added dynamically",
    test: "haha"
});
