interface Customer {
  name: string;
  email: string;
  phone: number;
  edm: boolean;
  remarks?: string; //optional
}

let c1:Customer = {
  name: "Customer 1",
  email: "c1@gmail.com",
  phone: 12345678,
  edm: false
};

let customers:Customer[] = [];
customers.push(c1);
customers.push({
  name: "Customer 2",
  email: "c2@gmail.com",
  phone: 2222222,
  edm: true,
  remarks: "added dynamically",
  // test: "haha" // cannot add this property
});