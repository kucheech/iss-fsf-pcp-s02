class Car {
  public make: string;
  public capacity: number;
  private serialNumber: string;

  //Constructer
  public constructor(make: string, capacity: number) {
    this.make = make;
    this.capacity = capacity;
    this.serialNumber = this.make + ":" + this.capacity;
  }

  public horn(times: number = 2) {
    for (let i = 0; i < times; i++) {
      console.log("[" + this.make + "]: horn");
    }
  }
}

//instantiation
let car1: Car = new Car("Suzuki", 1500); // create instance
car1.horn();

let car2: Car = new Car("Honda", 1500); // create instance
car2.horn(10);

let garage:Car[] = [car1, car2];
garage.forEach(car:Car) {
  car.horn();
};