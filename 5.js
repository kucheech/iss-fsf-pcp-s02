var Castle = /** @class */ (function () {
    function Castle(country, year) {
        this.country = country;
        this.year = year;
    }
    Castle.prototype.toString = function () {
        return "Built in " + this.country + " in " + this.year;
    };
    return Castle;
}());
var c = new Castle("France", 1567);
console.log(c.toString());
