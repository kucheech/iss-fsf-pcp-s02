var myname = 'fred';
// type inference
var age = 50;
console.log("My name is %s and am %d year old", myname, age);
